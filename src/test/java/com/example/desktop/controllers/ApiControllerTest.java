package com.example.desktop.controllers;

import com.example.desktop.models.CheckTask;
import com.example.desktop.models.auth.Auth;
import com.example.desktop.models.auth.Token;
import com.example.desktop.models.misc.DocumentOrigin;
import com.example.desktop.models.user.UserProfile;
import org.junit.jupiter.api.Test;

import java.io.UnsupportedEncodingException;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertNotNull;

class ApiControllerTest {

    private ApiController apiController;
    private Token token;
    private String STUDENT_LOGIN = "Anton@Afanasiev.com";
    private String STUDENT_PASSWORD = "DG7Uj6xp26FjFVyW";

    private String LECTURER_LOGIN = "lecturer1@diplom.com";
    private String LECTURER_PASSWORD = "5UWXq5urus5Vz6BR";

    @Test
    void auth() throws UnsupportedEncodingException {
        // given
        Auth auth = new Auth(STUDENT_LOGIN, STUDENT_PASSWORD, true);
        apiController = new ApiController();

        // when
        token = apiController.auth(auth);
        // then

        assertNotNull(token.getToken());
    }

    @Test
    void profile() {
        // given
        Auth auth = new Auth(STUDENT_LOGIN, STUDENT_PASSWORD, true);
        apiController = new ApiController();

        // when
        UserProfile us = apiController.profile(token);
        // then

        assertNotNull(us);
    }

    @Test
    void getStudentsListRooms() {
    }

    @Test
    void getStudentRoomById() {
    }

    @Test
    void getLecturerListRooms() {
    }

    @Test
    void getLecturerTasks() {
    }

    @Test
    void getCheckTasks() {
        // given
        apiController = new ApiController();
        Auth auth = new Auth(LECTURER_LOGIN, LECTURER_PASSWORD, true);
        token = apiController.auth(auth);

        // when
        List<CheckTask> checkTasks = apiController.getCheckTasks(token, "605f86a2e4c4273715ae3ead");
        // then

        assertNotNull(checkTasks);
    }


    @Test
    void getDocunebtByHash() {
        // given
        apiController = new ApiController();
        Auth auth = new Auth(LECTURER_LOGIN, LECTURER_PASSWORD, true);
        token = apiController.auth(auth);

        // when
        apiController.getDocunebtByHash(token, "629d99994d13ed5399bcd258");
        // then

    }

    @Test
    void uploadDocumentByPath() {
        //given
        apiController = new ApiController();
        Auth auth = new Auth(LECTURER_LOGIN, LECTURER_PASSWORD, true);
        token = apiController.auth(auth);

        // when
        String s = apiController.uplodeDocunebtByPath(token, "/home/cat/Desktop/Test document.pdf",
                DocumentOrigin.STUDENT_ANSWER_LAB.name());
        // then

        assertNotNull(s);
    }
}
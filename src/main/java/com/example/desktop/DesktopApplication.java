package com.example.desktop;

import com.example.desktop.controllers.AuthController;
import com.example.desktop.controllers.helper.ResizeHelper;
import com.example.desktop.i18n.I18N;
import com.example.desktop.i18n.Language;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

import java.io.IOException;
import java.util.ResourceBundle;

import static com.example.desktop.MFXApplicationResourcesLoader.loadURL;

public class DesktopApplication extends Application {

    private Scene scene;

    public static Parent root;

    @Override
    public void start(Stage stage) throws IOException {
        // CSSFX.start();

        //Check the SystemTray is supported
        /*if (!SystemTray.isSupported()) {
            System.out.println("SystemTray is not supported");
            return;
        }
        final PopupMenu popup = new PopupMenu();

        URL url = loadURL("img/icons8-100.png");
        //Image image = Toolkit.getDefaultToolkit().getImage(url);

        final TrayIcon trayIcon = new TrayIcon(image);

        final SystemTray tray = SystemTray.getSystemTray();

        // Create a pop-up menu components
        MenuItem aboutItem = new MenuItem("About");
        CheckboxMenuItem cb1 = new CheckboxMenuItem("Set auto size");
        CheckboxMenuItem cb2 = new CheckboxMenuItem("Set tooltip");
        Menu displayMenu = new Menu("Display");
        MenuItem errorItem = new MenuItem("Error");
        MenuItem warningItem = new MenuItem("Warning");
        MenuItem infoItem = new MenuItem("Info");
        MenuItem noneItem = new MenuItem("None");
        MenuItem exitItem = new MenuItem("Exit");

        //Add components to pop-up menu
        popup.add(aboutItem);
        popup.addSeparator();
        popup.add(cb1);
        popup.add(cb2);
        popup.addSeparator();
        popup.add(displayMenu);
        displayMenu.add(errorItem);
        displayMenu.add(warningItem);
        displayMenu.add(infoItem);
        displayMenu.add(noneItem);
        popup.add(exitItem);

        trayIcon.setPopupMenu(popup);


        try {
            tray.add(trayIcon);
        } catch (AWTException e) {
            System.out.println("TrayIcon could not be added.");
        }*/

        ResourceBundle bundle1 = I18N.getBundle(Language.defaultLocale());
        FXMLLoader loader = new FXMLLoader(loadURL("views/login-view.fxml"), bundle1);
        loader.setControllerFactory(c -> new AuthController(stage));
        root = loader.load();
        //root.getStylesheets().add(loadURL("css/light/main-light.css").toExternalForm());
        //root.getStylesheets().add(loadURL("css/dark/main-dark.css").toExternalForm());
        scene = new Scene(root);
        scene.setFill(Color.TRANSPARENT);
        // scene.getStylesheets().add(loadURL("css/light/main-light.css").toExternalForm());
        stage.initStyle(StageStyle.TRANSPARENT);
        Image icon = new Image(getClass().getResourceAsStream("img/icons8-100.png"));
        stage.getIcons().add(icon);
        stage.setScene(scene);
        ResizeHelper.addResizeListener(stage);
        stage.show();
    }

    public static void main(String[] args) {
        launch();
    }
}
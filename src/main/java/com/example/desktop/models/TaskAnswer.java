package com.example.desktop.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import javax.annotation.Generated;

@Generated("jsonschema2pojo")
public class TaskAnswer {

    @SerializedName("taskId")
    @Expose
    public String taskId;
    @SerializedName("taskName")
    @Expose
    public String taskName;
    @SerializedName("answerId")
    @Expose
    public String answerId;
    @SerializedName("taskStatus")
    @Expose
    public String taskStatus;
    @SerializedName("grade")
    @Expose
    public Integer grade;
    @SerializedName("gradeECTS")
    @Expose
    public Integer gradeECTS;

    public String getTaskId() {
        return taskId;
    }

    public void setTaskId(String taskId) {
        this.taskId = taskId;
    }

    public String getTaskName() {
        return taskName;
    }

    public void setTaskName(String taskName) {
        this.taskName = taskName;
    }

    public String getAnswerId() {
        return answerId;
    }

    public void setAnswerId(String answerId) {
        this.answerId = answerId;
    }

    public String getTaskStatus() {
        return taskStatus;
    }

    public void setTaskStatus(String taskStatus) {
        this.taskStatus = taskStatus;
    }

    public Integer getGrade() {
        return grade;
    }

    public void setGrade(Integer grade) {
        this.grade = grade;
    }

    public Integer getGradeECTS() {
        return gradeECTS;
    }

    public void setGradeECTS(Integer gradeECTS) {
        this.gradeECTS = gradeECTS;
    }

}
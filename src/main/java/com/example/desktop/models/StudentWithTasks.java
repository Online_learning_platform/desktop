package com.example.desktop.models;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import javax.annotation.Generated;
import java.util.List;

@Generated("jsonschema2pojo")
public class StudentWithTasks {

    @SerializedName("id")
    @Expose
    public String id;
    @SerializedName("email")
    @Expose
    public String email;
    @SerializedName("name")
    @Expose
    public String name;
    @SerializedName("taskAnswers")
    @Expose
    public List<TaskAnswer> taskAnswers = null;
    public int temp = 0;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<TaskAnswer> getTaskAnswers() {
        return taskAnswers;
    }

    public void setTaskAnswers(List<TaskAnswer> taskAnswers) {
        this.taskAnswers = taskAnswers;
    }

    public String getTaskStatus() {
        String str = taskAnswers.get(temp).getTaskStatus();
        temp++;
        if (temp >= taskAnswers.size()) temp = 0;
        return str;
    }
}
package com.example.desktop.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import javax.annotation.Generated;

@Generated("jsonschema2pojo")
public class Answer {

    @SerializedName("id")
    @Expose
    public String id;
    @SerializedName("student")
    @Expose
    public Student student;
    @SerializedName("grade")
    @Expose
    public Integer grade;
    @SerializedName("taskStatus")
    @Expose
    public String taskStatus;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Student getStudentId() {
        return student;
    }

    public void setStudentId(Student studentId) {
        this.student = studentId;
    }

    public Integer getGrade() {
        return grade;
    }

    public void setGrade(Integer grade) {
        this.grade = grade;
    }

    public String getTaskStatus() {
        return taskStatus;
    }

    public void setTaskStatus(String taskStatus) {
        this.taskStatus = taskStatus;
    }

}

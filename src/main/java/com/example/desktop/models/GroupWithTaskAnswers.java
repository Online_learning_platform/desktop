package com.example.desktop.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import javax.annotation.Generated;
import java.util.List;

@Generated("jsonschema2pojo")
public class GroupWithTaskAnswers {

    @SerializedName("id")
    @Expose
    public String id;
    @SerializedName("name")
    @Expose
    public String name;
    @SerializedName("students")
    @Expose
    public List<StudentWithTasks> students = null;

    @SerializedName("taskNames")
    @Expose
    public List<String> taskNames;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<StudentWithTasks> getStudents() {
        return students;
    }

    public void setStudents(List<StudentWithTasks> students) {
        this.students = students;
    }

    public List<String> getTaskNames() {
        return taskNames;
    }

    public void setTaskNames(List<String> taskNames) {
        this.taskNames = taskNames;
    }


}
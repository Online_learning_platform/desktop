package com.example.desktop.models.lecture;

import com.example.desktop.models.Subject;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import javax.annotation.Generated;
import java.util.List;


@Generated("jsonschema2pojo")
public class LecturerRoomShortResponse {

    @SerializedName("subjectlist")
    @Expose
    public List<Subject> subjectlist = null;

    public List<Subject> getSubjectlist() {
        return subjectlist;
    }

    public void setSubjectlist(List<Subject> subjectlist) {
        this.subjectlist = subjectlist;
    }

    public LecturerRoomShortResponse(List<Subject> subjectlist) {
        this.subjectlist = subjectlist;
    }

}
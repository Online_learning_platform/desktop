package com.example.desktop.models.lecture;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import javax.annotation.Generated;
import java.util.List;

@Generated("jsonschema2pojo")
public class LectureMaterial {

    @SerializedName("name")
    @Expose
    public String name;
    @SerializedName("description")
    @Expose
    public String description;
    @SerializedName("lectureDocuments")
    @Expose
    public List<DocumentResponse> lectureDocuments = null;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<DocumentResponse> getLectureDocuments() {
        return lectureDocuments;
    }

    public void setLectureDocuments(List<DocumentResponse> lectureDocuments) {
        this.lectureDocuments = lectureDocuments;
    }

}

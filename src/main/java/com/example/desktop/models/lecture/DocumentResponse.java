package com.example.desktop.models.lecture;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import javax.annotation.Generated;

@Generated("jsonschema2pojo")
public class DocumentResponse {

    @SerializedName("id")
    @Expose
    public String id;
    @SerializedName("fileName")
    @Expose
    public String fileName;
    @SerializedName("hashFileId")
    @Expose
    public String hashFileId;
    @SerializedName("size")
    @Expose
    public String size;
    @SerializedName("createdAt")
    @Expose
    public String createdAt;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getHashFileId() {
        return hashFileId;
    }

    public void setHashFileId(String hashFileId) {
        this.hashFileId = hashFileId;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

}
package com.example.desktop.models;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import javax.annotation.Generated;

@Generated("jsonschema2pojo")
public class CheckTask {

    @SerializedName("group")
    @Expose
    public GroupWithTaskAnswers group;

    public GroupWithTaskAnswers getGroup() {
        return group;
    }

    public void setGroup(GroupWithTaskAnswers group) {
        this.group = group;
    }

}
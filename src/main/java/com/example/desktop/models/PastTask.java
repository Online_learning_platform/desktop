package com.example.desktop.models;

import com.example.desktop.models.lecture.DocumentResponse;
import com.example.desktop.models.lecture.Lecturer;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import javax.annotation.Generated;
import java.util.List;

@Generated("jsonschema2pojo")
public class PastTask {

    @SerializedName("id")
    @Expose
    public String id;
    @SerializedName("answerTaskDocuments")
    @Expose
    public List<DocumentResponse> answerTaskDocuments = null;
    @SerializedName("taskStatus")
    @Expose
    public String taskStatus;
    @SerializedName("lecturerId")
    @Expose
    public Lecturer lecturerId;
    @SerializedName("name")
    @Expose
    public String name;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public List<DocumentResponse> getAnswerTaskDocuments() {
        return answerTaskDocuments;
    }

    public void setAnswerTaskDocuments(List<DocumentResponse> answerTaskDocuments) {
        this.answerTaskDocuments = answerTaskDocuments;
    }

    public String getTaskStatus() {
        return taskStatus;
    }

    public void setTaskStatus(String taskStatus) {
        this.taskStatus = taskStatus;
    }

    public Lecturer getLecturerId() {
        return lecturerId;
    }

    public void setLecturerId(Lecturer lecturerId) {
        this.lecturerId = lecturerId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}

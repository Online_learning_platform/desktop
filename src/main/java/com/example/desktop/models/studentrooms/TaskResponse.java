package com.example.desktop.models.studentrooms;

import com.example.desktop.models.lecture.DocumentResponse;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import javax.annotation.Generated;
import java.util.List;

@Generated("jsonschema2pojo")
public class TaskResponse {

    @SerializedName("id")
    @Expose
    public String id;
    @SerializedName("name")
    @Expose
    public String name;
    @SerializedName("description")
    @Expose
    public String description;
    @SerializedName("dateTo")
    @Expose
    public String dateTo;
    @SerializedName("taskType")
    @Expose
    public String taskType;
    @SerializedName("documentLecturer")
    @Expose
    public List<DocumentResponse> documentLecturer = null;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDateTo() {
        return dateTo;
    }

    public void setDateTo(String dateTo) {
        this.dateTo = dateTo;
    }

    public String getTaskType() {
        return taskType;
    }

    public void setTaskType(String taskType) {
        this.taskType = taskType;
    }

    public List<DocumentResponse> getDocumentLecturer() {
        return documentLecturer;
    }

    public void setDocumentLecturer(List<DocumentResponse> documentLecturer) {
        this.documentLecturer = documentLecturer;
    }

}

package com.example.desktop.models.studentrooms;

import com.example.desktop.models.lecture.LectureMaterial;
import com.example.desktop.models.lecture.LecturerShortResponse;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import javax.annotation.Generated;
import java.util.List;

@Generated("jsonschema2pojo")
public class StudentRoom {

    @SerializedName("id")
    @Expose
    public String id;
    @SerializedName("name")
    @Expose
    public String name;
    @SerializedName("description")
    @Expose
    public String description;
    @SerializedName("lecturers")
    @Expose
    public List<LecturerShortResponse> lecturers = null;
    @SerializedName("listTasks")
    @Expose
    public List<TaskResponse> listTasks = null;
    @SerializedName("lectureMaterials")
    @Expose
    public List<LectureMaterial> lectureMaterials = null;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<LecturerShortResponse> getLecturers() {
        return lecturers;
    }

    public void setLecturers(List<LecturerShortResponse> lecturerShortResponses) {
        this.lecturers = lecturerShortResponses;
    }

    public List<TaskResponse> getListTasks() {
        return listTasks;
    }

    public void setListTasks(List<TaskResponse> listTasks) {
        this.listTasks = listTasks;
    }


    public List<LectureMaterial> getLectureMaterials() {
        return lectureMaterials;
    }

    public void setLectureMaterials(List<LectureMaterial> lectureMaterials) {
        this.lectureMaterials = lectureMaterials;
    }

}
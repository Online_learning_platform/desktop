package com.example.desktop.models.misc;

public enum Role {
    ROLE_STUDENT,
    ROLE_LECTURER,
    ROLE_SUB_ADMINISTRATOR,
    ROLE_ADMINISTRATOR,
}

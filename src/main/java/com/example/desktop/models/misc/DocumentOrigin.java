package com.example.desktop.models.misc;

public enum DocumentOrigin {
    LECTURE,
    LECTURE_TASK,
    STUDENT_ANSWER_LAB
}

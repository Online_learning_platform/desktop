/*
 * Copyright (C) 2022 Parisi Alessandro
 * This file is part of MaterialFX (https://github.com/palexdev/MaterialFX).
 *
 * MaterialFX is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * MaterialFX is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MaterialFX.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.example.desktop;

import com.example.desktop.controllers.LecturerRoomController;
import com.example.desktop.controllers.LecturerSubjectController;
import com.example.desktop.controllers.RoomStudentController;
import com.example.desktop.controllers.SubjectController2;
import com.example.desktop.i18n.I18N;
import com.example.desktop.i18n.Language;
import com.example.desktop.models.auth.Token;
import com.example.desktop.models.lecture.Room;
import com.example.desktop.models.studentrooms.StudentRoom;
import javafx.animation.ParallelTransition;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.layout.StackPane;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.ResourceBundle;

/**
 * Utility class which manages the access to this project's assets.
 * Helps keeping the assets files structure organized.
 */
public class MFXApplicationResourcesLoader {

    private MFXApplicationResourcesLoader() {
    }

    public static URL loadURL(String path) {
        return MFXApplicationResourcesLoader.class.getResource(path);
    }

    public static String load(String path) {
        return loadURL(path).toString();
    }

    public static InputStream loadStream(String name) {
        return MFXApplicationResourcesLoader.class.getResourceAsStream(name);
    }

    public static void loadFxml(String path, StackPane pane) throws IOException {
        URL resource = loadURL(path);
        ResourceBundle bundle = I18N.getBundle(Language.defaultLocale());
        Parent fxml = FXMLLoader.load(resource, bundle);
        pane.getChildren().removeAll();
        pane.getChildren().setAll(fxml);

    }

    public static void loadFxml(String path, StackPane pane, StackPane pane1, Token token, ParallelTransition closeNav, Boolean st) throws IOException {
        URL resource = loadURL(path);
        ResourceBundle bundle = I18N.getBundle(Language.defaultLocale());
        FXMLLoader loader = new FXMLLoader(resource, bundle);
        if (st)
            loader.setControllerFactory(c -> new SubjectController2(token, pane1, closeNav));
        else
            loader.setControllerFactory(c -> new LecturerSubjectController(token, pane1, closeNav));
        //todo save token
        Parent fxml = loader.load();
        pane.getChildren().removeAll();
        pane.getChildren().setAll(fxml);

    }

    public static void loadFxml2(String path, StackPane pane, Token token, StudentRoom room) throws IOException {
        URL resource = loadURL(path);
        ResourceBundle bundle = I18N.getBundle(Language.defaultLocale());
        FXMLLoader loader = new FXMLLoader(resource, bundle);
        loader.setControllerFactory(c -> new RoomStudentController(token, room));
        //todo save token
        Parent fxml = loader.load();
        pane.getChildren().removeAll();
        pane.getChildren().setAll(fxml);

    }

    public static void loadFxmlLecturerRoom(String path, StackPane pane, Token token, Room room) throws IOException {
        URL resource = loadURL(path);
        ResourceBundle bundle = I18N.getBundle(Language.defaultLocale());
        FXMLLoader loader = new FXMLLoader(resource, bundle);
        loader.setControllerFactory(c -> new LecturerRoomController(token, room));
        //todo save token
        Parent fxml = loader.load();
        pane.getChildren().removeAll();
        pane.getChildren().setAll(fxml);

    }
}

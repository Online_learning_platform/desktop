package com.example.desktop.controllers;

import com.example.desktop.models.Subject;
import com.example.desktop.models.auth.Token;
import com.example.desktop.models.lecture.LecturerRoomShortResponse;
import com.example.desktop.models.lecture.Room;
import io.github.palexdev.materialfx.controls.MFXComboBox;
import javafx.animation.ParallelTransition;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;

import java.io.IOException;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;

import static com.example.desktop.MFXApplicationResourcesLoader.loadFxmlLecturerRoom;

public class LecturerSubjectController implements Initializable {

    private Token token;
    private ParallelTransition closeNav;
    private StackPane contentPane;
    private final static String ROOM = "views/lecturer/room-lecturer-view.fxml";
    @FXML
    private VBox subjectsRooms;

    private ApiController apiController;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        LecturerRoomShortResponse lecturerListRooms = apiController.getLecturerListRooms(token);
        List<Subject> subjectlist = lecturerListRooms.getSubjectlist();
        VBox vBox = new VBox();
        subjectlist.forEach(subject -> {
            Insets insets = new Insets(5);
            Insets inset = new Insets(2);

            ObservableList<Room> rooms = FXCollections.observableArrayList(subject.getRooms());

            Label label = new Label(subject.name);
            label.setAlignment(Pos.CENTER);
            label.setPadding(insets);
            label.setPrefHeight(50);

            MFXComboBox mfxComboBox = new MFXComboBox(rooms);
            mfxComboBox.setAlignment(Pos.CENTER);

            mfxComboBox.getSelectionModel().selectedItemProperty().addListener((options, oldValue, newValue) -> {
                Room room = (Room) mfxComboBox.getItems().get(mfxComboBox.getSelectedIndex());
                try {
                    closeNav.play();
                    loadFxmlLecturerRoom(ROOM, contentPane, token, room);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                //System.out.println(room.getId());
            });

            vBox.getChildren().add(label);
            vBox.getChildren().add(mfxComboBox);

        });
        subjectsRooms.getChildren().add(vBox);

    }

    public LecturerSubjectController(Token token, StackPane cPane, ParallelTransition closeNav) {
        this.token = token;
        this.contentPane = cPane;
        this.closeNav = closeNav;
        apiController = new ApiController();


    }
}

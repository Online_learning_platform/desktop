package com.example.desktop.controllers;

import com.example.desktop.i18n.I18N;
import com.example.desktop.i18n.Language;
import com.example.desktop.models.Student;
import com.example.desktop.models.TaskAnswer;
import com.example.desktop.models.auth.Token;
import com.example.desktop.models.lecture.Room;
import io.github.palexdev.materialfx.controls.MFXButton;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.VBox;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

import static com.example.desktop.MFXApplicationResourcesLoader.loadURL;

public class LecturerRoomController implements Initializable {

    private Token token;
    private Room room;
    private ApiController api;
    private final static String TASKS_URL = "views/lecturer/lecturer-tasks-view.fxml";
    private final static String ANSWERS_URL = "views/lecturer/lecturer-check-tasks-table-view.fxml";
    private final static String CHECK_ANSWER_URL = "views/lecturer/lecturer-check-task-view.fxml";
    @FXML
    private MFXButton labBtn;

    @FXML
    private MFXButton materialBtn;

    @FXML
    private MFXButton checkTaskBtn;

    @FXML
    private VBox MaterialVbox;

    @FXML
    private Label SubjNameTitle;

    @FXML
    private AnchorPane contentView;

    //private LecturerTasks listTasks;
    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        SubjNameTitle.setText(room.getName());
        labBtn.addEventHandler(MouseEvent.MOUSE_CLICKED, event -> {
            showTasks();
        });
        materialBtn.addEventHandler(MouseEvent.MOUSE_CLICKED, event -> {
            showMaterial();
        });
        checkTaskBtn.addEventHandler(MouseEvent.MOUSE_CLICKED, event -> {
            showAnswerTable();
        });
        showTasks();
    }

    public LecturerRoomController(Token token, Room room) {
        this.token = token;
        this.room = room;
    }

    public void showAnswerTable() {
        ResourceBundle bundle = I18N.getBundle(Language.defaultLocale());
        FXMLLoader loader = new FXMLLoader(loadURL(ANSWERS_URL), bundle);
        loader.setControllerFactory(c -> new LecturerCheckTasksTableView(token, room.getId(), this));
        //todo save token
        Parent fxml = null;
        try {
            fxml = loader.load();
            contentView.getChildren().clear();
            contentView.getChildren().add(fxml);
            Node node = contentView.getChildren().get(0);
            contentView.setBottomAnchor(node, Double.valueOf(0));
            contentView.setTopAnchor(node, Double.valueOf(0));
            contentView.setLeftAnchor(node, Double.valueOf(0));
            contentView.setRightAnchor(node, Double.valueOf(0));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void showStudentAnswer(TaskAnswer answer, String studentFIO) {
        ResourceBundle bundle = I18N.getBundle(Language.defaultLocale());
        FXMLLoader loader = new FXMLLoader(loadURL(CHECK_ANSWER_URL), bundle);
        loader.setControllerFactory(c -> new LecturerCheckTaskController(token, answer, studentFIO, this));
        //todo save token
        Parent fxml = null;
        try {
            fxml = loader.load();
            contentView.getChildren().clear();
            //contentView.getChildren().remove(2);
            contentView.getChildren().add(fxml);
            Node node = contentView.getChildren().get(0);
            contentView.setBottomAnchor(node, Double.valueOf(0));
            contentView.setTopAnchor(node, Double.valueOf(0));
            contentView.setLeftAnchor(node, Double.valueOf(0));
            contentView.setRightAnchor(node, Double.valueOf(0));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void showTasks() {
        ResourceBundle bundle = I18N.getBundle(Language.defaultLocale());
        FXMLLoader loader = new FXMLLoader(loadURL(TASKS_URL), bundle);
        loader.setControllerFactory(c -> new LecturerTasksController(token, room.getId()));
        //todo save token
        Parent fxml = null;
        try {
            fxml = loader.load();
            contentView.getChildren().clear();
            //contentView.getChildren().remove(2);
            contentView.getChildren().add(fxml);
            Node node = contentView.getChildren().get(0);
            contentView.setBottomAnchor(node, Double.valueOf(0));
            contentView.setTopAnchor(node, Double.valueOf(0));
            contentView.setLeftAnchor(node, Double.valueOf(0));
            contentView.setRightAnchor(node, Double.valueOf(0));
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public void showMaterial() {

    }
   /* public void setText(String text) {
        SubjNameTitle.setText(text);
    }

    public void addGroupToTask() {
        Stage stage = new Stage();
        ResourceBundle bundle = I18N.getBundle(Language.defaultLocale());
        FXMLLoader loader = new FXMLLoader(loadURL("views/lecturer/lecturer-add-groups-view.fxml"), bundle);
        loader.setControllerFactory(c -> new LecturerTasksController(this));
        Parent root = null;
        try {
            root = loader.load();
        } catch (IOException e) {
            e.printStackTrace();
        }

        stage.setTitle("Create folder");
        stage.setScene(new Scene(root, 400, 200));
        stage.initModality(Modality.APPLICATION_MODAL);
        stage.showAndWait();
    }*/


}

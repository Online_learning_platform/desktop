package com.example.desktop.controllers;

import com.example.desktop.i18n.I18N;
import com.example.desktop.i18n.Language;
import com.example.desktop.models.Group;
import com.example.desktop.models.auth.Token;
import com.example.desktop.models.lecture.LecturerTasks;
import io.github.palexdev.materialfx.controls.MFXButton;
import io.github.palexdev.materialfx.controls.MFXTableColumn;
import io.github.palexdev.materialfx.controls.MFXTableView;
import io.github.palexdev.materialfx.controls.cell.MFXTableRowCell;
import io.github.palexdev.materialfx.filter.StringFilter;
import javafx.collections.FXCollections;
import javafx.collections.MapChangeListener;
import javafx.collections.ObservableList;
import javafx.collections.ObservableMap;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.input.MouseEvent;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;

import java.io.IOException;
import java.net.URL;
import java.util.Comparator;
import java.util.List;
import java.util.ResourceBundle;

import static com.example.desktop.MFXApplicationResourcesLoader.loadURL;

public class LecturerTasksController implements Initializable {

    private List<LecturerTasks> listTasks;
    private ApiController api;
    private final static String NEW_TASKS_URL = "views/lecturer/lecturer-tasks-view.fxml";

    @FXML
    private MFXTableView<LecturerTasks> tasks;
    @FXML
    private MFXTableView<Group> groupsTable;
    @FXML
    private MFXButton addTaskBtn;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        setupTable();

        tasks.autosizeColumnsOnInitialization();

        addTaskBtn.addEventHandler(MouseEvent.MOUSE_CLICKED, event -> {
            Stage stage = new Stage();
            ResourceBundle bundle = I18N.getBundle(Language.defaultLocale());
            FXMLLoader loader = new FXMLLoader(loadURL("views/lecturer/lecturer-add-groups-view.fxml"), bundle);
            loader.setControllerFactory(c -> new LecturerAddNewTaskController());
            Parent root = null;
            try {
                root = loader.load();
            } catch (IOException e) {
                e.printStackTrace();
            }

            stage.setTitle("Create folder");
            stage.setScene(new Scene(root));
            stage.initModality(Modality.APPLICATION_MODAL);
            stage.setOnCloseRequest(new EventHandler<WindowEvent>() {
                public void handle(WindowEvent we) {
                    LecturerAddNewTaskController ladd = loader.getController();
                    if (ladd.getBool()) {

                    }
                }
            });
            ;
            stage.showAndWait();
        });
    }

    public LecturerTasksController(Token token, String roomid) {
        api = new ApiController();
        listTasks = api.getLecturerTasks(token, roomid);
    }


    private void setupTable() {
        MFXTableColumn<LecturerTasks> nameColumn = new MFXTableColumn<>("Имя", true, Comparator.comparing(LecturerTasks::getName));
        MFXTableColumn<LecturerTasks> surnameColumn = new MFXTableColumn<>("Тип", true, Comparator.comparing(LecturerTasks::getTaskType));
        //MFXTableColumn<Person> ageColumn = new MFXTableColumn<>("Age", true, Comparator.comparing(Person::getAge));

        nameColumn.setRowCellFactory(person -> new MFXTableRowCell<>(LecturerTasks::getName));
        surnameColumn.setRowCellFactory(person -> new MFXTableRowCell<>(LecturerTasks::getTaskType));
        /*ageColumn.setRowCellFactory(person -> new MFXTableRowCell<>(Person::getAge) {{
            setAlignment(Pos.CENTER_RIGHT);
        }});
        ageColumn.setAlignment(Pos.CENTER_RIGHT);*/

        tasks.getTableColumns().addAll(nameColumn, surnameColumn);
        tasks.getFilters().addAll(
                new StringFilter<>("Предмет", LecturerTasks::getName),
                new StringFilter<>("Препод", LecturerTasks::getTaskType)
        );
        tasks.setItems(FXCollections.observableArrayList(listTasks));

        tasks.getSelectionModel().selectionProperty().addListener(new MapChangeListener<Integer, LecturerTasks>() {
            @Override
            public void onChanged(Change<? extends Integer, ? extends LecturerTasks> change) {
                ObservableList<Group> groups = FXCollections.observableArrayList();
                ObservableMap<Integer, LecturerTasks> selection = tasks.getSelectionModel().getSelection();
                selection.forEach((id, listTasks) -> {
                    groups.addAll(listTasks.getGroups());
                });
                MFXTableColumn<Group> nameColumn = new MFXTableColumn<>("Группа", true, Comparator.comparing(Group::getName));

                nameColumn.setRowCellFactory(group -> new MFXTableRowCell<>(Group::getName));

                groupsTable.getTableColumns().clear();
                groupsTable.getTableColumns().addAll(nameColumn);
                groupsTable.getFilters().addAll(
                        new StringFilter<>("Группа", Group::getName)
                );
                groupsTable.setItems(groups);
            }
        });

    }



   /* public LecturerRoomController room;
    @FXML
    private MFXButton textButton;

    public LecturerTasksController(LecturerRoomController room) {
        this.room = room;
    }

    public void setText() {
        room.setText("pobeda");
        Stage stage = (Stage) textButton.getScene().getWindow();
        stage.close();
    }*/
}

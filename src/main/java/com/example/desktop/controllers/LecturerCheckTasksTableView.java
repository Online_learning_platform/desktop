package com.example.desktop.controllers;

import com.example.desktop.i18n.I18N;
import com.example.desktop.i18n.Language;
import com.example.desktop.models.CheckTask;
import com.example.desktop.models.GroupWithTaskAnswers;
import com.example.desktop.models.Student;
import com.example.desktop.models.StudentWithTasks;
import com.example.desktop.models.TaskAnswer;
import com.example.desktop.models.auth.Token;
import io.github.palexdev.materialfx.controls.MFXComboBox;
import io.github.palexdev.materialfx.controls.MFXTableColumn;
import io.github.palexdev.materialfx.controls.MFXTableView;
import io.github.palexdev.materialfx.controls.cell.MFXTableRowCell;
import io.github.palexdev.materialfx.filter.StringFilter;
import javafx.beans.property.ReadOnlyStringWrapper;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.util.Callback;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.ResourceBundle;
import java.util.stream.Collectors;

import static com.example.desktop.MFXApplicationResourcesLoader.loadURL;

public class LecturerCheckTasksTableView implements Initializable {

    private Token token;
    private List<CheckTask> checkTasks;
    private ApiController api;
    private LecturerRoomController main;

    private class TableRows {
        public List<StudentWithTasks> students;
        private List<String> FIO;
        private List<String> answers;
        public List<List<String>> rows;
        public int collcount;

        public TableRows() {
        }

        public TableRows(GroupWithTaskAnswers gr) {
            students = gr.getStudents();
            collcount = students.get(0).getTaskAnswers().size();
            int i = 0;
            answers = new ArrayList<>();
            rows = new ArrayList<>();
            for (StudentWithTasks st : students) {
                Collections.sort(st.getTaskAnswers(), new Comparator<TaskAnswer>() {
                    @Override
                    public int compare(TaskAnswer o1, TaskAnswer o2) {
                        return o1.getTaskId().compareTo(o1.getTaskId());
                    }
                });

                answers.add(st.getName());
                for (TaskAnswer tsk : st.taskAnswers) {
                    answers.add(tsk.taskStatus);
                }
                rows.add(new ArrayList<>(answers));
                answers.clear();
            }
        }

        public void setRow(Student student) {

        }
    }

    @FXML
    private TableView<List<String>> table;

    @FXML
    private MFXComboBox<String> comboBox;

    private TableRows tbl;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        List<String> gruops = new ArrayList<>();

        TableColumn<List<String>, String> nameColumn = new TableColumn<>("ФИО");
        nameColumn.setCellValueFactory(param -> new ReadOnlyStringWrapper(param.getValue().get(0)));
        //nameColumn.setAlignment(Pos.CENTER);
        nameColumn.setMinWidth(200);

        table.getColumns().add(nameColumn);
        tbl = new TableRows(checkTasks.get(0).getGroup());
        int i = 1;
        for (TaskAnswer str : tbl.students.get(0).taskAnswers) {
            TableColumn<List<String>, String> newColumn = new TableColumn<>(str.getTaskName());
            int finalI = i;
            newColumn.setCellValueFactory(param -> new ReadOnlyStringWrapper(param.getValue().get(finalI)));
            newColumn.setCellFactory(p -> {
                TableCell<List<String>, String> cell = new TableCell<>() {
                    @Override
                    public void updateItem(String item, boolean empty) {
                        super.updateItem(item, empty);
                        setText(empty ? null : getString());
                        setGraphic(null);
                    }

                    private String getString() {
                        return getItem() == null ? "" : getItem().toString();
                    }
                };
                cell.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
                    @Override
                    public void handle(MouseEvent event) {
                        if (event.getClickCount() > 1) {
                            //System.out.println("double clicked!");
                            TableCell c = (TableCell) event.getSource();
                            if (c.getText() != null) {
                                //System.out.println("Cell text: " + c.getTableView().getFocusModel().getFocusedCell().getColumn() + " " + c.getIndex());
                                var v = tbl.students.get(c.getIndex()).getTaskAnswers().get(c.getTableView()
                                        .getFocusModel().getFocusedCell().getColumn() - 1);//todo передать эти данные в новую форму
                                main.showStudentAnswer(v, tbl.students.get(c.getIndex()).getName());
                            }
                        }
                    }
                });

                return cell;
            });
            newColumn.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<List<String>, String>, ObservableValue<String>>() {

                @Override
                public ObservableValue<String> call(TableColumn.CellDataFeatures<List<String>, String> listStringCellDataFeatures) {
                    return new ReadOnlyStringWrapper(listStringCellDataFeatures.getValue().get(finalI));
                }
            });

            table.getColumns().add(newColumn);
            i++;
        }

        for (List<String> str : tbl.rows) {
            table.getItems().add(str);
        }

        comboBox.setItems(FXCollections.observableArrayList(gruops));
    }


    public LecturerCheckTasksTableView(Token token, String id, LecturerRoomController main) {
        this.token = token;
        api = new ApiController();
        checkTasks = api.getCheckTasks(token, id);
        this.main = main;
    }
}

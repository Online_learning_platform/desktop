package com.example.desktop.controllers;

import com.example.desktop.models.auth.Token;
import com.example.desktop.models.studentrooms.StudentRoom;
import com.example.desktop.models.studentrooms.StudentShortRoom;
import io.github.palexdev.materialfx.controls.MFXListView;
import io.github.palexdev.materialfx.controls.cell.MFXListCell;
import io.github.palexdev.materialfx.font.MFXFontIcon;
import io.github.palexdev.materialfx.utils.others.FunctionalStringConverter;
import javafx.animation.ParallelTransition;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.ObservableMap;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.StackPane;
import javafx.util.StringConverter;

import java.io.IOException;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;

import static com.example.desktop.MFXApplicationResourcesLoader.loadFxml2;

public class SubjectController2 implements Initializable {

    private final static String ROOM = "views/student/subject-student-view.fxml";
    private StackPane contentPane;
    @FXML
    private MFXListView<StudentShortRoom> custList;

    private ApiController apiController = new ApiController();
    private Token token;
    private ParallelTransition closeNav;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        List<StudentShortRoom> studentShortRoom = apiController.getStudentsListRooms(token);


        ObservableList<StudentShortRoom> studentShortRooms = FXCollections.observableArrayList(studentShortRoom);
        StringConverter<StudentShortRoom> converter = FunctionalStringConverter.
                to(person -> (person == null) ? "" : person.getName());


        custList.setItems(studentShortRooms);
        custList.setConverter(converter);
        custList.setCellFactory(person -> new PersonCellFactory(custList, person));
        custList.features().enableBounceEffect();
        custList.features().enableSmoothScrolling(0.5);

        custList.addEventHandler(MouseEvent.MOUSE_CLICKED, event -> {
            ObservableMap<Integer, StudentShortRoom> selection = custList.getSelectionModel().getSelection();
            StudentShortRoom roomsh = selection.get(0);
            StudentRoom room = apiController.getStudentRoomById(token, roomsh.id);
            //System.out.println(room);
            try {
                closeNav.play();
                loadFxml2(ROOM, contentPane, token, room);
            } catch (IOException e) {
                e.printStackTrace();
            }
        });

    }

    public SubjectController2(Token token, StackPane cPane, ParallelTransition closeNav) {
        this.token = token;
        contentPane = cPane;
        this.closeNav = closeNav;
    }

    public class PersonCellFactory extends MFXListCell<StudentShortRoom> {
        private final MFXFontIcon userIcon;

        public PersonCellFactory(MFXListView<StudentShortRoom> listView, StudentShortRoom data) {
            super(listView, data);

            userIcon = new MFXFontIcon("mfx-user", 18);
            userIcon.getStyleClass().add("user-icon");
            render(data);
        }

        @Override
        protected void render(StudentShortRoom data) {
            super.render(data);
            if (userIcon != null) getChildren().add(0, userIcon);
        }
    }
}

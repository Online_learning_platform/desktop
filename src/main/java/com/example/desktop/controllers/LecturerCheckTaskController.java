package com.example.desktop.controllers;

import com.example.desktop.models.TaskAnswer;
import com.example.desktop.models.auth.Token;
import io.github.palexdev.materialfx.controls.MFXButton;
import io.github.palexdev.materialfx.font.MFXFontIcon;
import javafx.event.EventHandler;
import javafx.event.EventType;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;

import java.net.URL;
import java.util.ResourceBundle;

public class LecturerCheckTaskController implements Initializable {
    private Token token;
    private TaskAnswer answer;
    private LecturerRoomController main;
    private String studentFIO;
    @FXML
    private MFXButton denyBtn;

    @FXML
    private Label labName;

    @FXML
    private MFXButton markBtn;

    @FXML
    private Label markLabel;

    @FXML
    private Label studentNameLabel;

    @FXML
    private Label statusLabel;

    @FXML
    private MFXFontIcon goBackButton;

    public LecturerCheckTaskController(Token token, TaskAnswer answer, String studentFIO, LecturerRoomController main) {
        this.token = token;
        this.answer = answer;
        this.main = main;
        this.studentFIO = studentFIO;
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        labName.setText(answer.getTaskName());
        studentNameLabel.setText(studentFIO);
        statusLabel.setText(statusLabel.getText() + answer.getTaskStatus());
        goBackButton.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent mouseEvent) {
                main.showAnswerTable();
            }
        });
        //if()
    }
}

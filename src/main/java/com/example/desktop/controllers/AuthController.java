package com.example.desktop.controllers;

import com.example.desktop.controllers.helper.ResizeHelper;
import com.example.desktop.i18n.I18N;
import com.example.desktop.i18n.Language;
import com.example.desktop.models.auth.Auth;
import com.example.desktop.models.auth.Token;
import com.example.desktop.models.misc.Role;
import com.example.desktop.models.user.UserProfile;
import io.github.palexdev.materialfx.controls.MFXCheckbox;
import io.github.palexdev.materialfx.controls.MFXPasswordField;
import io.github.palexdev.materialfx.font.MFXFontIcon;
import io.github.palexdev.materialfx.utils.ToggleButtonsUtil;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import javafx.stage.Window;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.prefs.Preferences;

import static com.example.desktop.MFXApplicationResourcesLoader.loadURL;

public class AuthController implements Initializable {

    private final static String STUDENT_MAIN = "views/student/main-view-student.fxml";
    private final static String LECTURER_MAIN = "views/lecturer/main-view-lecturer.fxml";
    private String login;
    private Token token;
    private String password;
    private boolean rememberMy = false;
    private Preferences pref;
    private String CURRENT_MAIN;
    private final Stage stage;
    private double xOffset;
    private double yOffset;
    private ToggleGroup toggleGroup;

    @FXML
    private TextField loginField;

    @FXML
    private MFXCheckbox rememberMyCheckbox;

    @FXML
    private MFXPasswordField passwordField;

    @FXML
    private AnchorPane ap;

    private ApiController apiController;

    /*Header Menu*/
    @FXML
    private HBox windowHeader;

    @FXML
    private MFXFontIcon closeIcon;

    @FXML
    private MFXFontIcon minimizeIcon;

    @FXML
    private MFXFontIcon expandIcon;

    private boolean isMin;


    public void onLoginButtonClick() {
        Auth auth = new Auth(loginField.getText(), passwordField.getText(), rememberMyCheckbox.isSelected());

        token = apiController.auth(auth);

        //todo work to refactor delite rememberMy, login, password
        pref.putBoolean("rememberMy", auth.getRememberMe());
        pref.put("login", auth.getLogin());
        pref.put("password", auth.getPassword());
        //pref.put("token", token.getToken());

        load();
    }

    public AuthController(Stage stage) {
        this.stage = stage;
        stage.setResizable(true);
        this.toggleGroup = new ToggleGroup();
        ToggleButtonsUtil.addAlwaysOneSelectedSupport(toggleGroup);
        this.apiController = new ApiController();
        pref = Preferences.userRoot().node(getClass().getName());

        if (pref.getBoolean("rememberMy", false)) {
            login = pref.get("login", "");
            password = pref.get("password", "");
            rememberMy = pref.getBoolean("rememberMy", true);
            String token1 = pref.get("token", null);
            if (token1 != null) {
                token = new Token(token1);
                // load();
            }
        }

        //isMin = true;
    }


    @FXML
    protected void movingWindow() {
       /* FXMLLoader loader = new FXMLLoader(getClass().getResource("MyGui.fxml"));
        Parent root = (Parent)loader.load();
        AuthController controller = (AuthController)loader.getController();
        controller.setStageAndSetupListeners(stage);*/

        Stage stage = (Stage) ap.getScene().getWindow();
    }


    @Override
    public void initialize(URL location, ResourceBundle resources) {
        String home = System.getProperty("user.home");
        File dir = new File(home + "/Downloads/OLP/");
        if (!dir.exists()) dir.mkdirs();

        stage.setMinHeight(400);
        stage.setMinWidth(600);
        loginField.setText(login);
        passwordField.setText(password);
        rememberMyCheckbox.setSelected(rememberMy);

        closeIcon.addEventHandler(MouseEvent.MOUSE_CLICKED, event -> Platform.exit());
        minimizeIcon.addEventHandler(MouseEvent.MOUSE_CLICKED, event -> ((Stage) stage.getScene().getWindow()).setIconified(true));
        expandIcon.addEventHandler(MouseEvent.MOUSE_CLICKED, event -> {
            Window window = stage.getScene().getWindow();
            if (isMin) {
                ((Stage) window).setMaximized(true);
                isMin = false;
            } else {
                ((Stage) window).setMaximized(false);
                isMin = true;
            }
        });


        windowHeader.setOnMousePressed(event -> {
            xOffset = stage.getX() - event.getScreenX();
            yOffset = stage.getY() - event.getScreenY();
        });
        windowHeader.setOnMouseDragged(event -> {
            stage.setX(event.getScreenX() + xOffset);
            stage.setY(event.getScreenY() + yOffset);
        });
    }

    private void loadMainView(String url, Token token, int role) {

        try {
            ResourceBundle bundle1 = I18N.getBundle(Language.defaultLocale());
            FXMLLoader loader = new FXMLLoader(loadURL(url), bundle1);
            loader.setControllerFactory(c -> new MainController(stage, token, role));
            Parent root = loader.load();
            Scene scene = new Scene(root);
            scene.setFill(Color.TRANSPARENT);
            stage.setScene(scene);
            ResizeHelper.addResizeListener(stage);
            stage.show();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

    }

    private void load() {
        CURRENT_MAIN = "views/main-view.fxml";
        if (token.token != null) {
            UserProfile profile = apiController.profile(token);
            Boolean l = false, s = false;
            for (Role role : profile.roles) {
                switch (role) {
                    case ROLE_STUDENT -> {
                        stage.setMinHeight(500);
                        stage.setMinWidth(800);
                        loadMainView(STUDENT_MAIN, token, 0);//CURRENT_MAIN = STUDENT_MAIN;
                    }
                    case ROLE_LECTURER -> {/*CURRENT_MAIN = LECTURER_MAIN;*/
                        stage.setMinHeight(700);
                        stage.setMinWidth(800);
                        l = true;
                    }
                    case ROLE_SUB_ADMINISTRATOR -> {
                        s = true;
                    }
                    //case ROLE_ADMINISTRATOR -> pathstr = LECTURER_MAIN;
                }
            }
            if (s == true && l == true) loadMainView(LECTURER_MAIN, token, 4);
            if (l == true) loadMainView(LECTURER_MAIN, token, 2);
            //if(s==true) loadMainView(LECTURER_MAIN, token, 2);
            //loadMainView(CURRENT_MAIN, token);
        }
    }
}
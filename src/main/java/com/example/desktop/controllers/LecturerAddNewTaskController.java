package com.example.desktop.controllers;

import com.example.desktop.models.lecture.LecturerTasks;
import io.github.palexdev.materialfx.controls.MFXButton;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.input.MouseEvent;

import java.net.URL;
import java.util.ResourceBundle;

public class LecturerAddNewTaskController implements Initializable {
    private Boolean addChoice;
    private LecturerTasks task;

    @FXML
    private MFXButton completeBtn;

    @FXML
    private MFXButton denyBtn;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        completeBtn.addEventHandler(MouseEvent.MOUSE_CLICKED, event -> {
            addChoice = true;
        });
        denyBtn.addEventHandler(MouseEvent.MOUSE_CLICKED, event -> {
            addChoice = false;
        });
    }

    public LecturerAddNewTaskController() {
        addChoice = false;
    }

    public Boolean getBool() {
        return addChoice;
    }

    public LecturerTasks getTask() {
        task = new LecturerTasks();

        return task;
    }
}

package com.example.desktop.controllers;

import com.example.desktop.models.lecture.DocumentResponse;
import com.example.desktop.models.lecture.LectureMaterial;
import com.example.desktop.models.studentrooms.TaskResponse;
import io.github.palexdev.materialfx.controls.MFXButton;
import io.github.palexdev.materialfx.controls.MFXScrollPane;
import io.github.palexdev.materialfx.effects.DepthLevel;
import io.github.palexdev.materialfx.enums.ButtonType;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;

import java.io.IOException;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;

public class MaterialStudentViewController implements Initializable {

    @FXML
    private Label DescriptionLable;

    @FXML
    private MFXScrollPane DescriptionScroll;

    @FXML
    private Label LectionName;

    @FXML
    private HBox list;

    private LectureMaterial lectm;
    private TaskResponse tascks;

    public void manu(MouseEvent actionEvent) throws IOException {
        System.out.println("DAAAAAAAAAAAAAAAAAAAA");
    }

    public MaterialStudentViewController(LectureMaterial material) {
        lectm = material;
    }

    public MaterialStudentViewController(TaskResponse material) {
        tascks = material;
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        if (lectm != null) {
            LectionName.setText(lectm.getName());
            DescriptionLable.setText(lectm.getDescription());


            List<DocumentResponse> lkm = lectm.getLectureDocuments();
            for (int i = 0; i < lkm.size(); i++) {
                DocumentResponse documentResponse = lkm.get(i);
                MFXButton mfxButton = new MFXButton(documentResponse.getFileName());
                mfxButton.setButtonType(ButtonType.RAISED);
                mfxButton.setDepthLevel(DepthLevel.LEVEL3);

                mfxButton.addEventHandler(MouseEvent.MOUSE_CLICKED, event -> {

                });

                list.getChildren().add(mfxButton);
            }
        }
        if (tascks != null) {
            LectionName.setText(tascks.getName());
            DescriptionLable.setText(tascks.getDescription());


            /*List<DocumentResponse> lkm = lectm.getLectureDocuments();
            for (int i = 0; i < lkm.size(); i++) {
                DocumentResponse documentResponse = lkm.get(i);
                MFXButton mfxButton = new MFXButton(documentResponse.getFileName());
                mfxButton.setButtonType(ButtonType.RAISED);
                mfxButton.setDepthLevel(DepthLevel.LEVEL3);

                mfxButton.addEventHandler(MouseEvent.MOUSE_CLICKED, event -> {

                });

                list.getChildren().add(mfxButton);
            }*/
        }
    }


}

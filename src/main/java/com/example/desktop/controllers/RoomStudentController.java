package com.example.desktop.controllers;

import com.example.desktop.i18n.I18N;
import com.example.desktop.i18n.Language;
import com.example.desktop.models.auth.Token;
import com.example.desktop.models.lecture.LectureMaterial;
import com.example.desktop.models.lecture.LecturerShortResponse;
import com.example.desktop.models.studentrooms.StudentRoom;
import com.example.desktop.models.studentrooms.TaskResponse;
import io.github.palexdev.materialfx.controls.MFXButton;
import io.github.palexdev.materialfx.controls.MFXRectangleToggleNode;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.control.Label;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;

import java.io.IOException;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;

import static com.example.desktop.MFXApplicationResourcesLoader.loadURL;

public class RoomStudentController implements Initializable {

    private Token token;
    private StudentRoom room;
    @FXML
    private StackPane ContentRoom;

    @FXML
    private MFXButton LabBtn;

    @FXML
    private MFXButton MaterialBtn;

    @FXML
    private VBox MaterialVbox;

    @FXML
    private Label SubjFIOTitle;

    @FXML
    private Label SubjNameTitle;

    @FXML
    private MFXRectangleToggleNode radiomaterial;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        SubjNameTitle.setText(room.getName());
        String FIO = "";
        for (LecturerShortResponse lect : room.getLecturers()) {
            FIO += lect.getName();
            FIO += ", ";
        }

        SubjFIOTitle.setText(FIO);

        LabBtn.setRippleColor(Color.WHITE);

        //radiomaterial.setLabelLeadingIcon(MFXFontIcon.getRandomIcon(16, Color.BLACK));
        //radiomaterial.setLabelTrailingIcon(MFXFontIcon.getRandomIcon(16, Color.BLACK));

//-------------------------------------------------------------------------------------------------------------
        OpenMaterials();
    }

    private void OpenMaterials() {
        MaterialVbox.getChildren().clear();
        List<LectureMaterial> lkm = room.getLectureMaterials();
        for (int i = 0; i < lkm.size(); i++) {
            //URL cardURL = getClass().getResource("views/material-student-view.fxml");
            //ResourceBundle bundle1 = I18N.getBundle(Language.defaultLocale());
            try {
                ResourceBundle bundle = I18N.getBundle(Language.defaultLocale());
                FXMLLoader loader = new FXMLLoader(loadURL("views/student/material-student-view.fxml"), bundle);

                int finalI = i;
                loader.setControllerFactory(c -> new MaterialStudentViewController(lkm.get(finalI)));

                Parent materialroom = loader.load();

                MaterialVbox.getChildren().add(materialroom);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private void Opentasks() {
        MaterialVbox.getChildren().clear();
        List<TaskResponse> lkm = room.getListTasks();
        for (int i = 0; i < lkm.size(); i++) {
            //URL cardURL = getClass().getResource("views/material-student-view.fxml");
            //ResourceBundle bundle1 = I18N.getBundle(Language.defaultLocale());
            try {
                ResourceBundle bundle = I18N.getBundle(Language.defaultLocale());
                FXMLLoader loader = new FXMLLoader(loadURL("views/student/material-student-view.fxml"), bundle);

                int finalI = i;
                loader.setControllerFactory(c -> new MaterialStudentViewController(lkm.get(finalI)));

                Parent materialroom = loader.load();

                MaterialVbox.getChildren().add(materialroom);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public void labrabshow() {
        Opentasks();
    }

    public void matshow() {
        OpenMaterials();
    }

    public RoomStudentController(Token token, StudentRoom room) {
        this.token = token;
        this.room = room;
    }
}

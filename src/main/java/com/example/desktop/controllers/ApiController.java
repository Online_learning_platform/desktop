package com.example.desktop.controllers;

import com.example.desktop.models.CheckTask;
import com.example.desktop.models.PastTask;
import com.example.desktop.models.auth.Auth;
import com.example.desktop.models.auth.Token;
import com.example.desktop.models.lecture.LecturerRoomShortResponse;
import com.example.desktop.models.lecture.LecturerTasks;
import com.example.desktop.models.studentrooms.StudentRoom;
import com.example.desktop.models.studentrooms.StudentShortRoom;
import com.example.desktop.models.user.UserProfile;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.client.HttpClients;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Type;
import java.util.List;

public class ApiController {

    private final String URL_MAIN = "http://localhost:8080/api/";
    private final String URL_LOGIN = "auth/login";
    private final String URL_PROFILE = "/user/profile";


    /**
     * STUDENT
     **/
    private final String URL_STUDENT_LIST_ROOMS = "/student/get/rooms";
    private final String URL_STUDENT_ROOM = "/student/get/%s";


    /**
     * LECTURER
     **/
    private final String URL_LECTURER_LIST_ROOMS = "/lecturer/get/rooms";
    private final String URL_LECTURER_LIST_TASKS = "/lecturer/task/%s";//todo change task to tasks
    private final String URL_LECTURER_CHECK_TASKS = "/lecturer/check/%s";

    private final String URL_LECTURER_CHECK_TASK = "lecturer/passTask/%s/%d";


    /**
     * DOCUMENTS
     **/
    private final String URL_DOWNLOAD_DOCUMENT = "/document/file/%s";
    private final String URL_DOWNLOAD_DOCUMENT1 = "document/download/%s";
    private final String URL_UPLOAD_DOCUMENT = "document/upload";


    private CloseableHttpClient client;
    private HttpPost httpPost;
    private HttpGet httpGet;
    private Gson gson;

    public Token auth(Auth auth) {
        gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();

        String personJson = gson.toJson(auth);
        String response = post(URL_LOGIN, personJson);

        Token token = gson.fromJson(response, Token.class);

        return token;
    }

    public UserProfile profile(Token token) {
        String response = get(URL_PROFILE, token.getToken());

        gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
        UserProfile userProfile = gson.fromJson(response, UserProfile.class);

        return userProfile;
    }

    /*
     * STUDENTS
     */

    public List<StudentShortRoom> getStudentsListRooms(Token token) {
        String response = get(URL_STUDENT_LIST_ROOMS, token.getToken());
        gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
        Type listType = new TypeToken<List<StudentShortRoom>>() {
        }.getType();
        List<StudentShortRoom> studentShortRooms = new Gson().fromJson(response, listType);
        return studentShortRooms;
    }

    public StudentRoom getStudentRoomById(Token token, String id) {
        String response = get(String.format(URL_STUDENT_ROOM, id), token.getToken());

        gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
        StudentRoom room = gson.fromJson(response, StudentRoom.class);

        return room;
    }


    /*
     * LECTURE
     */

    public LecturerRoomShortResponse getLecturerListRooms(Token token) {
        String response = get(URL_LECTURER_LIST_ROOMS, token.getToken());

        LecturerRoomShortResponse studentShortRooms = new Gson().fromJson(response, LecturerRoomShortResponse.class);

        return studentShortRooms;
    }

    public List<LecturerTasks> getLecturerTasks(Token token, String roomId) {
        String response = get(String.format(URL_LECTURER_LIST_TASKS, roomId), token.getToken());
        Type listType = new TypeToken<List<LecturerTasks>>() {
        }.getType();
        List<LecturerTasks> lecturerTasks = new Gson().fromJson(response, listType);

        return lecturerTasks;
    }

    public List<CheckTask> getCheckTasks(Token token, String roomId) {
        String response = get(String.format(URL_LECTURER_CHECK_TASKS, roomId), token.getToken());
        Type listType = new TypeToken<List<CheckTask>>() {
        }.getType();
        List<CheckTask> lecturerTasks = new Gson().fromJson(response, listType);

        return lecturerTasks;
    }

    public PastTask pastTask(Token token, String answerId, Integer gradeECTS) {
        String response = post(String.format(URL_LECTURER_CHECK_TASK, answerId, gradeECTS), token.getToken());
        return new Gson().fromJson(response, PastTask.class);
    }


    public String uplodeDocunebtByPath(Token token, String path, String type) {
        String s = postB1(URL_UPLOAD_DOCUMENT, type, path, token.getToken());

        return s;
    }

    public void getDocunebtByHash(Token token, String hash) {
        getB(String.format(URL_DOWNLOAD_DOCUMENT1, hash), null);

    }

    private String post(String url, String json) {
        StringEntity entity = null;
        try {
            entity = new StringEntity(json);
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException(e);
        }
        CloseableHttpResponse response;

        client = HttpClients.createDefault();

        httpPost = new HttpPost(URL_MAIN + url);
        httpPost.setEntity(entity);
        httpPost.setHeader("Accept", "application/json");
        httpPost.setHeader("Content-type", "application/json");

        try {
            response = client.execute(httpPost);
            if (response.getStatusLine().getStatusCode() == 200) {
                String responseString = new BasicResponseHandler().handleResponse(response);
                client.close();
                return responseString;
            } else {
                client.close();
                throw new RuntimeException();
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private String get(String url, String token) {
        CloseableHttpResponse response;

        client = HttpClients.createDefault();

        httpGet = new HttpGet(URL_MAIN + url);
        httpGet.setHeader("Accept", "application/json");
        httpGet.setHeader("Content-type", "application/json");
        if (!token.isEmpty()) {
            httpGet.setHeader("Authorization", "Bearer " + token);
        }

        try {
            response = client.execute(httpGet);
            if (response.getStatusLine().getStatusCode() == 200) {
                String responseString = new BasicResponseHandler().handleResponse(response);
                client.close();
                return responseString;
            } else {
                client.close();
                throw new RuntimeException();
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private int getB(String url, String token) {
        CloseableHttpResponse response;

        client = HttpClients.createDefault();

        httpGet = new HttpGet(URL_MAIN + url);
        httpGet.setHeader("Accept", "application/json");
        httpGet.setHeader("Content-type", "application/json");
        /*if (!token.isEmpty()) {
            httpGet.setHeader("Authorization", "Bearer " + token);
        }*/

        try {
            response = client.execute(httpGet);
            HttpEntity entity = response.getEntity();
            if (response.getStatusLine().getStatusCode() == 200) {
                if (entity != null) {
                    String name = response.getFirstHeader("Content-Disposition").getValue();
                    String fileName = name.replaceFirst("(?i)^.*filename=\"([^\"]+)\".*$", "$1");
                    String home = System.getProperty("user.home");
                    FileOutputStream fos = new FileOutputStream(home + "/Downloads/OLP/" + fileName);
                    entity.writeTo(fos);
                    fos.close();
                }
                client.close();
                return 0;
            } else {
                client.close();
                throw new RuntimeException();
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private String postB1(String url, String type, String filePath, String token) {


        HttpEntity entity = MultipartEntityBuilder.create()
                .addPart("file", new FileBody(new File(filePath)))
                .build();

        HttpPost request = new HttpPost("http://localhost:8080/api/document/upload?origin=STUDENT_ANSWER_LAB");

        request.setHeader("Authorization", "Bearer " + token);

        request.setEntity(entity);


        client = HttpClientBuilder.create().build();
        try {

            HttpResponse response = client.execute(request);
            if (response.getStatusLine().getStatusCode() == 200) {
                String responseString = new BasicResponseHandler().handleResponse(response);

                return responseString;
            } else {
                throw new RuntimeException();
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

}

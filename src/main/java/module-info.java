module com.example.desktop {
    requires javafx.controls;
    requires javafx.fxml;
    requires javafx.graphics;
    requires MaterialFX;
    requires org.apache.httpcomponents.httpclient;
    requires org.apache.httpcomponents.httpcore;
    requires java.prefs;
    requires java.annotation;
    requires com.google.gson;
    requires httpmime;
    requires spring.web;
    requires spring.core;


    opens com.example.desktop to javafx.fxml;
    opens com.example.desktop.controllers to javafx.fxml;
    exports com.example.desktop;
    exports com.example.desktop.models.auth to com.google.gson;
    exports com.example.desktop.models.user to com.google.gson;
    exports com.example.desktop.models.dto to com.google.gson;
    exports com.example.desktop.models to com.google.gson;
    exports com.example.desktop.models.misc to com.google.gson;
    exports com.example.desktop.models.studentrooms to com.google.gson;
    exports com.example.desktop.models.lecture to com.google.gson;
    exports com.example.desktop.config;
    opens com.example.desktop.config to javafx.fxml;
    exports com.example.desktop.controllers;
}